package com.dolgiy.coding.errorGenerating;

/**
 * Interface for classes that can generate {@link byte[]}.
 */
public interface ByteArrayGenerator {
    byte[] generate();
}

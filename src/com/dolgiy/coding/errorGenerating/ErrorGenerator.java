package com.dolgiy.coding.errorGenerating;

import java.util.BitSet;
import java.util.Random;

/**
 * Represent generator sequences of {@link byte} in which byte contains only one 1 with given probabilities and on random place.
 */
public class ErrorGenerator implements ByteArrayGenerator{

    private final double probabilityOfOneError;
    private final int bytesCount;

    /**
     * Initialize an object using given parameters.
     * @param probabilityOfOneError Gets the probability of error in each byte.
     * @param bytesCount Gets the number of byte that will return {@link #generate()} method.
     * @throws IllegalArgumentException if probabilityOfOneError more then 1 or less then 0.
     */
    public ErrorGenerator(double probabilityOfOneError, int bytesCount) {
        if(probabilityOfOneError > 1 || probabilityOfOneError < 0){
            throw new IllegalArgumentException("Probability can't be more then 1 or less then 0. Actual value: " + probabilityOfOneError);
        }
        this.probabilityOfOneError = probabilityOfOneError;
        this.bytesCount = bytesCount;
    }


    /**
     * Generates sequence of {@link byte}.
     * @return Returns generated sequence.
     */
    @Override
    public byte[] generate() {
        byte[] result = new byte[bytesCount];
        Random randomError = new Random();
        BitSet errorPosition;
        for(int i = 0; i < bytesCount; i++){
            if(randomError.nextDouble() < probabilityOfOneError){
                errorPosition = new BitSet(8);
                errorPosition.set(randomError.nextInt(0xfffffff) % 8,true); // Put '1' on the random position between 0 and 7.
                result[i] = errorPosition.toByteArray()[0];
            }
            else{
                result[i] = 0;
            }
        }
        return result;
    }
}

package com.dolgiy.coding.coders;

import java.util.BitSet;

/**
 * Represents class that provides Hamming coding algorithm.
 */
public class HammingCoder implements StringCoder, StringDecoder {

    private StringCoder basicCoder;
    private StringDecoder basicDecoder;
    private boolean isErrorCorrectionEnabled;

    /**
     * Constructs class with given parameters.
     * @param basicCoder Gets the basic coder. {@link StringCoder#code(String)} will be invoked before {@link #code(String)}.
     * @param basicDecoder Gets the basic decoder. {@link StringDecoder#decode(byte[])} will be invoked after {@link #decode(byte[])}.
     */
    public HammingCoder(StringCoder basicCoder, StringDecoder basicDecoder) {
        this.basicCoder = basicCoder;
        this.basicDecoder = basicDecoder;
        this.isErrorCorrectionEnabled = true;
    }

    /**
     *Constructs class with given parameters.
     * @param basicCoder Gets the basic coder. {@link StringCoder#code(String)} will be invoked before {@link #code(String)}.
     * @param basicDecoder Gets the basic decoder. {@link StringDecoder#decode(byte[])} will be invoked after {@link #decode(byte[])}.
     * @param isErrorCorrectionEnabled If this parameter equals false, {@link HammingCoder} won't correct the message
     *                                 in {@link #decode(byte[])} method. This parameter affect only {@link #decode(byte[])} method.
     */
    public HammingCoder(StringCoder basicCoder, StringDecoder basicDecoder, boolean isErrorCorrectionEnabled) {
        this.basicCoder = basicCoder;
        this.basicDecoder = basicDecoder;
        this.isErrorCorrectionEnabled = isErrorCorrectionEnabled;
    }

    /**
     * Codes string as bytes and adds checking bits to byte sequence.
     * @param text Gets initial text.
     * @return Returns sequence of byte with checking bits.
     */
    @Override
    public byte[] code(String text) {
        byte[] tempResult = basicCoder.code(text);
        BitSet messageBits = BitSet.valueOf(tempResult);
        byte[] codedMessage = new byte[tempResult.length * 2];
        BitSet b; // One byte in coded message.
        byte[] temp;
        for(int i = 3, j = 0; i < tempResult.length * 8; i += 4){
            b = new BitSet(8);
            b.set(0, messageBits.get(i-3)); // A
            b.set(1, messageBits.get(i-2)); // B
            b.set(2, messageBits.get(i-1)); // C
            b.set(3, b.get(0) ^ b.get(1) ^ b.get(2)); // x
            b.set(4, messageBits.get(i));   // D
            b.set(5, b.get(0) ^ b.get(1) ^ b.get(4)); // y
            b.set(6, b.get(0) ^ b.get(2) ^ b.get(4)); // z
            b.set(7, b.get(0) ^ b.get(1) ^ b.get(2) ^ b.get(3) ^ b.get(4) ^ b.get(5) ^ b.get(6) ^ b.get(7)); // parity bit.
            temp = b.toByteArray();
            if(temp.length == 0){
                codedMessage[j++] = 0;
            }
            else{
                codedMessage[j++] = temp[0];
            }

        }
        return codedMessage;
    }

    /**
     * Decodes bytes sequence as Hamming code.
     * @param code Gets the Hamming code.
     * @return Returns the string taken from input code.
     */
    @Override
    public String decode(byte[] code) {
        byte[] result = new byte[code.length / 2];
        BitSet b;
        BitSet resultByte;
        byte[] temp;
        for(int i = 1, j = 0; i < code.length; i+=2){
            resultByte = new BitSet(8);
            b = BitSet.valueOf(new byte[] {code[i - 1]});
            if(b.get(7) != b.get(6) ^ b.get(5) ^ b.get(4) ^ b.get(3) ^ b.get(2) ^ b.get(1) ^ b.get(0)){
                if(isErrorCorrectionEnabled){
                    b = correctByte(b);
                }
            }
            resultByte.set(0,b.get(0));
            resultByte.set(1,b.get(1));
            resultByte.set(2,b.get(2));
            resultByte.set(3,b.get(4));

            b = BitSet.valueOf(new byte[] {code[i]});
            if(b.get(7) != b.get(6) ^ b.get(5) ^ b.get(4) ^ b.get(3) ^ b.get(2) ^ b.get(1) ^ b.get(0)){
                if(isErrorCorrectionEnabled){
                    b = correctByte(b);
                }
            }
            resultByte.set(4,b.get(0));
            resultByte.set(5,b.get(1));
            resultByte.set(6,b.get(2));
            resultByte.set(7,b.get(4));

            temp = resultByte.toByteArray();
            if(temp.length == 0){
                result[j++] = 0;
            }
            else{
                result[j++] = temp[0];
            }
        }

        return basicDecoder.decode(result);
    }

    /**
     * Corrects one error in one byte, using Hamming algorithm.
     * @param byteAsBits Gets the wrong byte.
     * @return Returns the correct byte.
     */
    private BitSet correctByte(BitSet byteAsBits){
        BitSet errorPosition = new BitSet(3);
        errorPosition.set(2, byteAsBits.get(0) ^ byteAsBits.get(1) ^ byteAsBits.get(2) ^ byteAsBits.get(3));
        errorPosition.set(1, byteAsBits.get(0) ^ byteAsBits.get(1) ^ byteAsBits.get(4) ^ byteAsBits.get(5));
        errorPosition.set(0, byteAsBits.get(0) ^ byteAsBits.get(2) ^ byteAsBits.get(4) ^ byteAsBits.get(6));
        byte[] temp = errorPosition.toByteArray();
        byte position = 0;
        if(temp.length != 0){
            position = temp[0];
        }
        if(position > 0){
            byteAsBits.flip(7 - position);
        }
        return byteAsBits;
    }

}

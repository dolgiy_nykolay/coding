package com.dolgiy.coding.coders;

import java.util.Arrays;
import java.util.BitSet;

/**
 * Represents class that provides iteration coding algorithm.
 */
public class IterationMethodCoder implements StringCoder, StringDecoder {

    private StringCoder basicCoder;
    private StringDecoder basicDecoder;
    private boolean isErrorCorrectionEnabled;

    /**
     * Constructs class with given parameters.
     * @param basicCoder Gets the basic coder. {@link StringCoder#code(String)} will be invoked before {@link #code(String)}.
     * @param basicDecoder Gets the basic decoder. {@link StringDecoder#decode(byte[])} will be invoked after {@link #decode(byte[])}.
     */
    public IterationMethodCoder(StringCoder basicCoder, StringDecoder basicDecoder) {
        this.basicCoder = basicCoder;
        this.basicDecoder = basicDecoder;
        this.isErrorCorrectionEnabled = true;
    }

    /**
     *Constructs class with given parameters.
     * @param basicCoder Gets the basic coder. {@link StringCoder#code(String)} will be invoked before {@link #code(String)}.
     * @param basicDecoder Gets the basic decoder. {@link StringDecoder#decode(byte[])} will be invoked after {@link #decode(byte[])}.
     * @param isErrorCorrectionEnabled If this parameter equals false, {@link HammingCoder} won't correct the message
     *                                 in {@link #decode(byte[])} method. This parameter affect only {@link #decode(byte[])} method.
     */
    public IterationMethodCoder(StringCoder basicCoder, StringDecoder basicDecoder, boolean isErrorCorrectionEnabled) {
        this.basicCoder = basicCoder;
        this.basicDecoder = basicDecoder;
        this.isErrorCorrectionEnabled = isErrorCorrectionEnabled;
    }

    /**
     * Codes string as bytes and adds checking bits to byte sequence.
     * @param text Gets initial text.
     * @return Returns sequence of byte with checking bits.
     */
    @Override
    public byte[] code(String text) {
        byte[] tempResult = basicCoder.code(text);
        BitSet message = BitSet.valueOf(tempResult);
        BitSet result = new BitSet();
        int j = 0;
        for (int i = 0; i < tempResult.length * 8;) {
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i - 1) ^ message.get(i - 2) ^ message.get(i - 3));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i - 1) ^ message.get(i - 2) ^ message.get(i - 3));
        }

        long r = result.toLongArray()[0];
        byte c1 = (byte) (r % 11);
        byte c2 = (byte) (r % 13);
        byte c3 = (byte) (r % 14);
        byte c4 = (byte) (r % 15);
        byte[] control = new byte[2];
        control[0] = (byte) (c1 << 4);
        control[0] = (byte) (control[0] | c2);
        control[1] = (byte) (c3 << 4);
        control[1] = (byte) (control[1] | c4);

        byte[] rs = result.toByteArray();
        rs = Arrays.copyOf(rs, rs.length + 2);
        rs[rs.length - 2] = control[0];
        rs[rs.length - 1] = control[1];
        return rs;
    }

    /**
     * Decodes bytes sequence as Iteration code.
     * @param code Gets the Iteration code.
     * @return Returns the string taken from input code.
     */
    @Override
    public String decode(byte[] code) {
        byte c1 = (byte) ((code[code.length - 2] & 0xf0) >>> 4);
        byte c2 = (byte) (code[code.length - 2] & 0x0f);
        byte c3 = (byte) ((code[code.length - 1] & 0xf0) >>> 4);
        byte c4 = (byte) (code[code.length - 1] & 0x0f);

        code = Arrays.copyOf(code, code.length - 2);
        if(isErrorCorrectionEnabled){
            // TODO: Add invocation of checking and correction method.
        }
        BitSet message = BitSet.valueOf(code);
        BitSet result = new BitSet();
        for(int i = 0, j = 0; i < code.length * 8; i++){
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
            result.set(j++,message.get(i++));
        }

        String tempResult = basicDecoder.decode(result.toByteArray());
        return tempResult;
    }
}

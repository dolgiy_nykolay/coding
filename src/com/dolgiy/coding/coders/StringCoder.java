package com.dolgiy.coding.coders;

/**
 * Interface for classes that can convert {@link String} to {@link byte[]} using some custom rules.
 */
public interface StringCoder {
    byte[] code(String text);
}

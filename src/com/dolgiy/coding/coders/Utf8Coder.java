package com.dolgiy.coding.coders;

import java.nio.charset.Charset;

/**
 * Represents UTF-8 coder and decoder for string.
 */
public class Utf8Coder implements StringCoder, StringDecoder {

    /**
     * Converts {@link String} to {@link byte[]} in UTF-8 encoding.
     * @param text Gets the text.
     * @return Returns {@link byte[]} in UTF-8 encoding.
     */
    @Override
    public byte[] code(String text) {
        return text.getBytes(Charset.forName("UTF-8"));
    }

    /**
     * Converts {@link byte[]} to {@link String} using UTF-8 encoding.
     * @param code Gets {@link byte[]}.
     * @return Returns decoded {@link String}.
     */
    @Override
    public String decode(byte[] code) {
        return new String(code, Charset.forName("UTF-8"));
    }
}

package com.dolgiy.coding.coders;


/**
 * Interface for classes that can convert {@link byte[]} to {@link String} using some custom rules.
 */
public interface StringDecoder {
    String decode(byte[] code);
}

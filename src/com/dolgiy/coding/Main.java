package com.dolgiy.coding;

import com.dolgiy.coding.coders.*;
import com.dolgiy.coding.errorGenerating.ByteArrayGenerator;
import com.dolgiy.coding.errorGenerating.ErrorGenerator;


import java.io.*;


public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Phrase: /end will stop the program.");
        Utf8Coder utf8Coder = new Utf8Coder();
        StringCoder baseCoder = utf8Coder;
        StringDecoder baseDecoder = utf8Coder;
        System.out.print("Enter error probability or /end: ");
        String input;
        long hammingCodingTime;
        long iterationCodingTime;
        long hammingDecodingTime;
        long iterationDecodingTime;
        int errorLength;
        double errorProb;
        while(!(input = reader.readLine().trim()).equals("/end")){
            try{
                errorProb = new Double(input);
            }
            catch (NumberFormatException e){
                System.out.println("Input error: " + e.getLocalizedMessage());
                System.out.print("Enter error probability or /end: ");
                continue;
            }
            System.out.print("Enter the test message: ");
            String initialText = reader.readLine().trim();

            HammingCoder hammingCoder = new HammingCoder(baseCoder, baseDecoder);
            HammingCoder hammingCoderWithoutCorrection = new HammingCoder(baseCoder, baseDecoder,false);
            IterationMethodCoder iterationMethodCoder = new IterationMethodCoder(baseCoder,baseDecoder);
            IterationMethodCoder iterationMethodCoder1WithoutCorrection = new IterationMethodCoder(baseCoder,baseDecoder,false);

            hammingCodingTime = System.currentTimeMillis();
            byte[] hammingCodedMessage = hammingCoder.code(initialText);
            hammingCodingTime = System.currentTimeMillis() - hammingCodingTime;

            iterationCodingTime = System.currentTimeMillis();
            byte[] iterationMethodCodedMessage = iterationMethodCoder.code(initialText);
            iterationCodingTime = System.currentTimeMillis() - iterationCodingTime;

            errorLength = Math.max(iterationMethodCodedMessage.length, hammingCodedMessage.length);
            ByteArrayGenerator errorGenerator;
            try{
                errorGenerator = new ErrorGenerator(errorProb,errorLength);
            }
            catch (IllegalArgumentException e){
                System.out.println("Input error: " + e.getLocalizedMessage());
                System.out.print("Enter error probability or /end: ");
                continue;
            }

            byte[] errors = errorGenerator.generate();

            for(int i = 0; i < hammingCodedMessage.length; i++){
                hammingCodedMessage[i] = (byte) (hammingCodedMessage[i] ^ errors[i]);
            }
            for(int i = 0; i < iterationMethodCodedMessage.length; i++){
                iterationMethodCodedMessage[i] = (byte) (iterationMethodCodedMessage[i] ^ errors[i]);
            }

            // Hamming method check.
            hammingDecodingTime = System.currentTimeMillis();
            String hammingDecodedString = hammingCoder.decode(hammingCodedMessage);
            hammingDecodingTime = System.currentTimeMillis() - hammingDecodingTime;
            System.out.println("-Hamming method: ");
            System.out.println("  Coding time: " + hammingCodingTime + "ms.");
            System.out.println("  Decoding with correction time: " + hammingDecodingTime + "ms.");
            System.out.printf("  Decoded string with correction: %s\n", hammingDecodedString);
            hammingDecodingTime = System.currentTimeMillis();
            String hammingDecodedWithoutCorrection = hammingCoderWithoutCorrection.decode(hammingCodedMessage);
            hammingDecodingTime = System.currentTimeMillis() - hammingDecodingTime;
            System.out.println("  Decoding without correction time: " + hammingDecodingTime + "ms.");
            System.out.printf("  Decoded string without correction: %s\n", hammingDecodedWithoutCorrection);
            System.out.println("-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -");

            // Iteration method check.
            iterationDecodingTime = System.currentTimeMillis();
            String iterationDecodedString = iterationMethodCoder.decode(iterationMethodCodedMessage);
            iterationDecodingTime = System.currentTimeMillis() - iterationDecodingTime;
            System.out.println("-Iteration method: ");
            System.out.println("  Coding time: " + iterationCodingTime + "ms.");
            System.out.println("  Decoding with correction time: " + iterationDecodingTime+ "ms.");
            System.out.printf("  Decoded string with correction: %s\n", iterationDecodedString);
            iterationDecodingTime = System.currentTimeMillis();
            String iterationDecodedWithoutCorrection = iterationMethodCoder1WithoutCorrection.decode(iterationMethodCodedMessage);
            iterationDecodingTime = System.currentTimeMillis() - iterationDecodingTime;
            System.out.println("  Decoding without correction time: " + iterationDecodingTime + "ms.");
            System.out.printf("  Decoded string without correction: %s\n", iterationDecodedWithoutCorrection );

            System.out.println("- - - - - - - - - - - - - - -");
            System.out.print("Enter error probability or /end: ");
        }
    }
}
